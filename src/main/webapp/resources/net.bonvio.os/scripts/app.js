/**
 * Created by mil on 11.03.2015.
 */

'use strict';

/* App Module */

var netBonvioOs = angular.module('netBonvioOs', [
    'ngRoute',
    'ngMaterial'
]).config(configuration);

configuration.$inject = ['$routeProvider', '$locationProvider'];

function configuration($routeProvider, $locationProvider) {
    $routeProvider.

        when('/', {
            templateUrl: '/resources/net.bonvio.os/partials/main.html',
            controller: 'mainCtrl'
        }).
        when('/login', {
            templateUrl: '/resources/net.bonvio.os/partials/login.html'//,
            //controller: 'loginCtrl'
        }).

        otherwise({
            redirectTo: '/'//,
            //controller: 'mainCtrl'
        });
    $locationProvider.html5Mode(true);
}
