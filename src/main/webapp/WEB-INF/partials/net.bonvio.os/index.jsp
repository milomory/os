<!doctype html>
<html lang="ru" ng-app="netBonvioOs">
<head>

    <script src="/resources/net.bonvio.os/bower_components/angular/angular.min.js"></script>
    <script src="/resources/net.bonvio.os/bower_components/angular-route/angular-route.min.js"></script>
    <script src="/resources/net.bonvio.os/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="/resources/net.bonvio.os/bower_components/angular-aria/angular-aria.min.js"></script>
    <script src="/resources/net.bonvio.os/bower_components/angular-material/angular-material.min.js"></script>

    <link rel="stylesheet" href="/resources/net.bonvio.os/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/resources/net.bonvio.os/bower_components/angular-material/angular-material.min.css">
    <link rel="stylesheet" href="/resources/net.bonvio.os/styles/main.css">

    <script src="/resources/net.bonvio.os/scripts/app.js"></script>
    <script src="/resources/net.bonvio.os/scripts/controllers/login.js"></script>
    <script src="/resources/net.bonvio.os/scripts/controllers/main.js"></script>

    <title>OS App</title>
    <base href="/">

</head>
<body layout="column">



<%--<md-sidenav class="md-sidenav-left md-whiteframe-z2" md-component-id="right">
    <md-toolbar class="md-theme-light">
        <h1 class="md-toolbar-tools">
            <span>${message}</span>
        </h1>
    </md-toolbar>
    <md-content ng-controller="RightCtrl" class="md-padding">
        <md-button ng-click="close()" class="md-primary">
            Close Sidenav Right
        </md-button>
    </md-content>
</md-sidenav>--%>



<section>
    <ng-view></ng-view>
</section>

</body>
</html>