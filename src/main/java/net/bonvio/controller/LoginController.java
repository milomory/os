package net.bonvio.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by mil on 11.03.2015.
 */

@Controller
@RequestMapping("/login")
public class LoginController {
    public LoginController(){
        System.out.println("create controller");

    }

    @RequestMapping(method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        System.out.println("printWelcome");

        model.addAttribute("message", "loginPage");
        return "index";

    }
}