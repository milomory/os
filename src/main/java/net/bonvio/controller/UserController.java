package net.bonvio.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import net.bonvio.dao.MockUserDao;

/**
 * Created by mil on 12.03.15.
 */

/**
 * Контроллер для работы с пользователями.
 */
@Controller
public class UserController {
    private MockUserDao userDao = new MockUserDao();

    @RequestMapping("/users")
    public ModelAndView listUsers() {
        return new ModelAndView("users", "users", userDao.getAllUsers());
    }
}