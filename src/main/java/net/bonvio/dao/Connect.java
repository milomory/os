package net.bonvio.dao;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.*;

/**
 * Created by mil on 10.03.2015.
 */
public class Connect {

    private static final String URL = "jdbc:mysql://ares.beget.ru:3306/mil_denis";
    private static final String LOGIN = "mil_denis";
    private static final String PASSWORD = "y3l0l3k0r";

    public static void main(String[] args) {
        Connection connection;

        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from mandarinko_base;");

            while (resultSet.next()){
                String resultSetString = resultSet.getString(1);
                System.out.println(resultSetString);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
